# WindozeXP-enhanced

Forked from WindozeXP

Better throbber and decrypt field


## install
- copy to /usr/share/plymouth/themes/WindozeXP
- chown -R root the folder
- sudo plymouth-set-default-theme -R WindozeXP

## conf

See also: [Arch Wiki](https://wiki.archlinux.org/title/Plymouth)

```
/etc/plymouth/plymouthd.conf:

...
[Daemon]
Theme=WindozeXP
ShowDelay=1 //Add some delay to remove flicker
DeviceScale=2 //Integer factor for scaling
UseFirmwareBackground=true //Might be needed to show firmware logo with BGRT theme
```

### scaling
The Logo Image is 640x480.
Use the following integer factors on bigger screens:
 - 480px-959px vertical height: 1
   - 480, 600, 720, 768, 900
 - 960px-1919px vertical height: 2
   - 960, 1024, 1050, 1080, 1200, 1280, 1440, 1600
 - 1920px-3839px vertical height: 3
   - 2160, 2400
 - 3840px-7679px vertical height: 4
   - 4096, 4800
 - ...